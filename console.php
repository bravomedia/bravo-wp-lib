<?php

if(php_sapi_name() != 'cli') {
    die;
}

require __DIR__ . '/Command.php';

\bravo\Command::run();
