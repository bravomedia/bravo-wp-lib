<?php

namespace bravo;

class WP {
    public static $scripts = array();
    public static $sidebars = array();
    public static $menus = array();
    public static $widgets = array();
    public static $viewArgs = array();
    public static $scriptsInFooter = true;

    public static function init() {
        if (!function_exists('of_get_option')) {
            function of_get_option($name, $default = false) {
                $optionsframework_settings = get_option('optionsframework');
                // Gets the unique option id
                $option_name = $optionsframework_settings['id'];
                if (get_option($option_name)) {
                    $options = get_option($option_name);
                }
                if (isset($options[$name])) {
                    return $options[$name];
                } else {
                    return $default;
                }
            }
        }

        add_action('wp_enqueue_scripts', function() {
            foreach(WP::$scripts['css'] as $handle => $src) {
                wp_enqueue_style($handle, WP::getScriptUrl($src));
            }
            foreach(WP::$scripts['js']  as $handle => $opts) {
                $depends = isset($opts['depends']) ? isset($opts['depends']) : array();
                $footer = isset($opts['footer']) ? isset($opts['footer']) : static::$scriptsInFooter;
                $src = is_array($opts) ? $opts[0] : $opts;
                wp_enqueue_script($handle, WP::getScriptUrl($src), $depends, false, $footer);
            }
        });

        foreach(static::$sidebars as $sidebar) {
            register_sidebar($sidebar);
        }

        add_action('init', function() {
            register_nav_menus(WP::$menus);

            // Bugherd
            if($key = of_get_option('bugherd_apikey')) {
                add_action('wp_footer', function() {
                    echo "<script type='text/javascript'>
                        (function (d, t) {
                          var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
                          bh.type = 'text/javascript';
                          bh.src = '//www.bugherd.com/sidebarv2.js?apikey=".$key."';
                          s.parentNode.insertBefore(bh, s);
                          })(document, 'script');
                        </script>";
                });
            }

            // Include Google Analytics code
            if($analytics_key = of_get_option('google_analytics')) {
                add_action('wp_head', function() use($analytics_key) {
                    printf("<script type='text/javascript'>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                    ga('create', '%s', 'auto');
                    ga('send', 'pageview');

                    </script>", $analytics_key);
                });
            }

        });

        add_action('widgets_init', function() {
            foreach(WP::$widgets as $widget) {
                register_widget($widget);
            }
        });
    }

    public static function display($layout, $template, array $args = array()) {
        echo static::render($layout, array_merge($args, array(
            'args' => $args,
            'layout' => $layout,
            'template' => $template,
            'content' => function() use($template, $args) {
                return WP::render($template, $args);
            },
        )));
    }

    public static function render($template, array $args = array()) {
        $_template_ = $template;
        if($_template_[0] != '/') {
            $_template_ = get_template_directory() . '/' . $_template_;
        }
        if(!isset($args['post'])) {
            $args['post'] = $GLOBALS['post'];
        }
        $args = array_merge(static::$viewArgs, $args);
        extract($args);
        ob_start();
        require($_template_);
        $content = ob_get_clean();
        return $content;
    }

    public static function getScriptUrl($src) {
        if(strpos($src, '~/') === 0) {
            $src = substr($src, 1);
            $file = get_template_directory() . $src;
            if(is_file($file)) {
                $src .= '?' . substr(dechex(filemtime($file)), -4);
            }
            return get_template_directory_uri() . $src;
        }
        return $src;
    }

    public static function debug() {
        foreach(func_get_args() as $arg) {
            echo '<pre>';
            var_dump($arg);
            echo '</pre>';
        }
    }
}
