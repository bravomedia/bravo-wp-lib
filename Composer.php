<?php

namespace bravo;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

if(php_sapi_name() != 'cli') {
    die;
}

require __DIR__ . '/Command.php';

class Composer {

    static function postUpdate(Event $event) {
        Command::run('update', array('event' => $event));
    }

    static function postInstall(Event $event) {
        $composer = $event->getComposer();
        // do stuff
    }

    static function postPackageInstall(PackageEvent $event) {
        $installedPackage = $event->getOperation()->getPackage();
        // do stuff
    }
}
