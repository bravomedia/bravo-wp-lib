<?php

namespace bravo;

if(php_sapi_name() != 'cli') {
    die;
}

/**
 * @todo Config dialog:
 *       - Select theme
 *       - Create clients (lock down if adminize is enabled)
 * @todo Cmd to fix file permissions
 */
class Command {
    private $_config;
    public static $options;

    public static function usage() {
        global $argv;
        printf("%s <action> [params...]\n", $argv[0]);
    }

    public static function run() {
        global $argv;
        if(empty($argv[1])) {
            static::usage();
            die;
        }
        return static::runAction($argv[1], array_slice($argv, 2));
    }

    public static function runAction($action, array $args) {
        try {
            static::$options = require(__DIR__ . '/options.php');
            $action = str_replace('-', '_', $action);
            $command = new static;
            if(!method_exists($command, $action)) {
                throw new \Exception(sprintf('Action %s not found', $action));
            }
            return call_user_func(array($command, $action), $args);
        } catch(\Exception $ex) {
            echo $ex;
            static::usage();
        }
    }

    public static function getRootDir() {
        return realpath(__DIR__ . '/../../..');
    }

    public static function getVendorDir() {
        return static::getRootDir() . '/vendor/';
    }

    public static function getContentDir() {
        return static::getRootDir() . '/content/';
    }

    public function __construct() {
        # load config if
    }

    public function getConfig() {
        if($this->_config === null) {
            $this->_config = array();
            $configFile = $this->getConfigFile();
            if(is_file($configFile)) {
                $config = require $configFile;
                if(is_array($config)) {
                    $this->_config = $config;
                }
            }
        }
        return $this->_config;
    }

    public function getConfigFile() {
        return static::getRootDir() . '/.config.php';
    }

    public function saveConfig($config) {
        if(is_array($config)) {
            $configFile = static::getConfigFile();
            $defaultConfig = static::getDefaultConfig();
            echo "Write config $configFile:\n";
            #var_dump($config);
            $config = array_merge($defaultConfig, $config);
            file_put_contents($configFile, '<?php return ' . var_export($config, true) . ';');
            $this->_config = $config;
        } else {
            echo "Error: Config is null\n";
        }
    }

    public function getDefaultConfig() {
        return array(
            'DB_NAME' => 'bravowp',
            'DB_USER' => 'bravowp',
            'DB_PASSWORD' => 'bravowp',
            'DB_HOST' => 'localhost',
            'DB_PREFIX' => 'bravowp_',
            'WP_ENV' => 'development',
            'SAVEQUERIES' => true,
            'WP_DEBUG' => true,
            'AUTH_KEY' => static::generateRandomString(50),
            'SECURE_AUTH_KEY' => static::generateRandomString(50),
            'LOGGED_IN_KEY' => static::generateRandomString(50),
            'NONCE_KEY' => static::generateRandomString(50),
            'AUTH_SALT' => static::generateRandomString(50),
            'SECURE_AUTH_SALT' => static::generateRandomString(50),
            'LOGGED_IN_SALT' => static::generateRandomString(50),
            'NONCE_SALT' => static::generateRandomString(50),
        );
    }

    public function isDev($config) {
        return isset($config['WP_ENV']) && $config['WP_ENV'] === 'development';
    }

    public function help(array $args = array()) {
        static::usage();
    }

    public function init(array $args = array()) {
        $this->init_config();
        $this->init_users();
    }

    public function init_config(array $args = array()) {
        $config = $this->getConfig();
        $defaultConfig = static::getDefaultConfig();

        // Database & init
        do {
            $retry = false;

            foreach($defaultConfig as $key => $default) {
                if(empty($config[$key])) {
                    printf("%s (default=%s): ", $key, $default);
                    $result = trim(readline());
                    $config[$key] = strlen($result) ? $result : $default;
                }
            }

            $this->saveConfig($config);

            if(!$this->check_db_connection()) {
                echo "Fail, change config and retry? (Y/n)\n";
                if(readline() == 'n') {
                    throw new \Exception('DB connection failed');
                }

                foreach(array('DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DB_HOST', 'DB_PREFIX') as $key) {
                    $defaultConfig[$key] = $config[$key];
                    unset($config[$key]);
                }
                $retry = true;
            }
        } while($retry);

        // Settings
        // TODO: readline()
        if(isset(static::$options['settings'])) {
            foreach(static::$options['settings'] as $key => $settings) {
                if(!isset($config['settings'][$key])) {
                    $config['settings'][$key] = $settings['default'];
                }
            }
        }

        $this->saveConfig($config);
    }

    public function init_users(array $args = array()) {
        $config = $this->getConfig();
    }

    public function ínstall(array $args = array()) {
    }

    public function update(array $args = array()) {
    }

    public function status(array $args = array()) {
        $this->status_config();
        $this->status_db();
    }

    public function status_db(array $args = array()) {
        printf("DB connection: %s\n", $this->check_db_connection() ? "OK" : "FAILED");
    }

    public function status_config(array $args = array()) {
        printf("Config status: %s\n", $this->getConfig() !== null ? "OK" : "INCOMPLETE");
    }

    public function production_config(array $args = array()) {
        $config = $this->getConfig();
        $config['DB_HOST'] = 'localhost';
        $config['SAVEQUERIES'] = false;
        $config['WP_DEBUG'] = false;
        $config['WP_ENV'] = 'production';
        echo "<?php\n";
        echo "// Do not add this file to revision control!\n";
        echo "// Copy and place in .config.php on production server or deployment tool\n";
        echo "return " . var_export($config, true) . ";\n\n";
    }

    protected function check_db_connection() {
        $config = $this->getConfig();

        // Check DB
        try {
            echo "Checking database connection\n";
            $dsn = sprintf('mysql:host=%s', $config['DB_HOST']);
            $pdo = new \PDO($dsn, $config['DB_USER'], $config['DB_PASSWORD']);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            echo "Connected\n";
            $dblist = array_map(function($row) {
                return $row['Database'];
            }, iterator_to_array($pdo->query('SHOW DATABASES')));
            if(!in_array($config['DB_NAME'], $dblist)) {
                echo "Creating database\n";
                $pdo->query('CREATE DATABASE ' . $config['DB_NAME']);
                echo "Database created, visit the website to complete the installation\n";
            }
            echo "Done\n";
            return true;
        } catch(\PDOException $ex) {
            echo $ex->getMessage();
            echo "\n";
        }
        return false;
    }

    protected static function copyDir($source, $dest) {
        echo "Copy $source -> $dest";
        mkdir($dest, 0755);
        foreach (
         $iterator = new \RecursiveIteratorIterator(
          new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
          \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
          if ($item->isDir()) {
            mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
          } else {
            copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
          }
        }
    }
    
    protected static function generateRandomKey($length = 32) {
        if (!extension_loaded('mcrypt')) {
            throw new Exception('The mcrypt PHP extension is not installed.');
        }
        $bytes = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
        if ($bytes === false) {
            throw new Exception('Unable to generate random bytes.');
        }
        return $bytes;
    }

    protected static function generateRandomString($length = 32)
    {
        $bytes = static::generateRandomKey($length);
        return strtr(substr(base64_encode($bytes), 0, $length), '+/', '_-');
    }
}